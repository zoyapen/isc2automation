import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.remote.server.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import org.junit.After as After
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

Date today = new Date()

String todaysDate = today.format('MM-dd-yyyy')

String currentTime = today.format('hh-mm-ss')

String actualName = new String((('CA' + todaysDate) + '-') + currentTime)

int SHORT_TIMEOUT = 10

int TIMEOUT = 20

int LONG_TIMEOUT = 30

String currentDirectory = System.getProperty('user.dir')

WebUI.openBrowser(GlobalVariable.URL)

WebDriver driver = DriverFactory.getWebDriver()

WebDriverWait waitForVisibilityOfElement = new WebDriverWait(driver, LONG_TIMEOUT)

WebDriverWait shortWaitForVisibilityOfElement = new WebDriverWait(driver, SHORT_TIMEOUT)

WebUI.click(findTestObject('SiteCore/CreateAccountPage/SignInButtonOnHomePage'))

WebUI.click(findTestObject('SiteCore/CreateAccountPage/a_Create An Account'))

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/firstNameField'), actualName)
println("First Name:-" + actualName)

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/lastNameField'), 'AutomatedCreatedAccount')
println("Last Name:-AutomatedCreatedAccount")

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/emailField'), ((('Ca' + todaysDate) + '-') + currentTime) + '@mailinator.com')

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmEmailField'), ((('Ca' + todaysDate) + '-') + currentTime) + 
    '@mailinator.com')

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/paswordField'), ((('Ca' + todaysDate) + '-') + currentTime) + '@mailinator.com')

WebUI.setText(findTestObject('SiteCore/CreateAccountPage/confirmPasswordField'), ((('Ca' + todaysDate) + '-') + currentTime) + 
    '@mailinator.com')

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-createNewAccount/Scenario-createNewAccount/enteredAccountCreationDetails-') + 
    todaysDate) + '_') + currentTime) + '.PNG')

WebUI.click(findTestObject('SiteCore/CreateAccountPage/privacyPolicyCheckbox'))

WebUI.click(findTestObject('SiteCore/CreateAccountPage/createNewAccountButton'))

waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//header[@id=\'site-header\']/div/div/div/ul/li[3]/a')))

WebUI.click(findTestObject('SiteCore/CreateAccountPage/VerifyTheAccountHolderName'))

ExpectedName = WebUI.getText(findTestObject('SiteCore/CreateAccountPage/VerifyTheAccountHolderName'))

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-createNewAccount/Scenario-createNewAccount/signInSuccessful-') + 
    todaysDate) + '_') + currentTime) + '.PNG')

WebUI.verifyElementText(findTestObject('SiteCore/CreateAccountPage/VerifyTheAccountHolderName'), 'HI, ' + actualName)

WebUI.closeBrowser()

