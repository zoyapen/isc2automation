import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.server.DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.junit.After
import com.kms.katalon.core.webui.driver.DriverFactory

Date today = new Date()
String todaysDate = today.format('MM-dd-yyyy')
String currentTime = today.format('hh-mm-ss')
int SHORT_TIMEOUT = 10;
int TIMEOUT = 20;
int LONG_TIMEOUT = 30;
String currentDirectory = System.getProperty("user.dir");

WebUI.openBrowser(GlobalVariable.URL)
WebDriver driver = DriverFactory.getWebDriver()
WebDriverWait waitForVisibilityOfElement= new WebDriverWait(driver, LONG_TIMEOUT)
WebDriverWait shortWaitForVisibilityOfElement= new WebDriverWait(driver, SHORT_TIMEOUT)

WebUI.waitForPageLoad(30)

WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))
WebUI.waitForElementVisible(findTestObject('SiteCore/CertificationsPages/RegistrationPage/Page_Cybersecurity and IT Security Certific_16a664/a_Register for an Exam                     _59a42d'),
	10)
WebUI.click(findTestObject('SiteCore/CertificationsPages/RegistrationPage/Page_Cybersecurity and IT Security Certific_16a664/a_Register for an Exam                     _59a42d'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/RegistrationPage/Page_Register for Cybersecurity Exam  Find _9de3f9/h1_Register for Exam'), 
    'Register for Exam')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

//WebUI.waitForPageLoad(30)

//WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_Register for Cybersecurity Exam  Find _9de3f9/a_Endorsement                              _e0276b'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Endorsement  Online Endorsement Applic_ed4e5e/h1_Get Endorsed'), 
    'Get Endorsed')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

//WebUI.waitForElementVisible(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'), 30)

//WebUI.waitForPageLoad(30)

//WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_Endorsement  Online Endorsement Applic_ed4e5e/a_Member Verification                      _02e1f7'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_/h1_Verify Certification or Designation'), 
    'Verify Certification or Designation')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

//WebUI.waitForElementVisible(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'), 30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('Verify that navigation to Certification page is successful/Page_(ISC) Digital Badges from Acclaim  (ISC)/a_Digital Badges                           _fa77f7'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_(ISC) Digital Badges from Acclaim  (ISC)/h1_(ISC) Digital Badges from Acclaim'), 
    '(ISC)² Digital Badges from Acclaim')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)


WebUI.waitForPageLoad(30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_Cybersecurity and IT Security Certific_16a664/a_CISSP                                    _4e0bb0'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Cybersecurity Certification CISSP - Ce_5dfe81/h1_CISSP  The Worlds Premier Cybersecurity _cb3f16'), 
    'CISSP – The World\'s Premier Cybersecurity Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Certification  SSCP - Syst_6d35f5/a_SSCP                                     _c7aa66'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_IT Security Certification  SSCP - Syst_6d35f5/h1_SSCP  The Premier Security Administrator_593585'), 
    'SSCP – The Premier Security Administrator Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Certification  SSCP - Syst_6d35f5/a_CCSP                                     _cb9bc3'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Cloud Security Certification  CCSP - C_4e65b7/h1_CCSP  The Industrys Premier Cloud Securi_a84189'), 
    'CCSP – The Industry’s Premier Cloud Security Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

//WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_Cloud Security Certification  CCSP - C_4e65b7/a_CAP                                      _daeb5a'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Security Authorization Certification  _3626fd/h1_CAP  Security Assessment and Authorizati_55689e'), 
    'CAP – Security Assessment and Authorization Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Architect Engineer and Man_20c9f5/a_CISSP Concentrations                     _d8be5c'))

WebUI.verifyTextPresent('CISSP Concentrations', false)

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

//WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_Security Authorization Certification  _3626fd/a_CSSLP                                    _c4b737'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/CSSLPPage/csslpPageTitle'), 'CSSLP – The Industry’s Premier Secure Software Development Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

//WebUI.waitForElementVisible(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'), 30)

WebUI.waitForPageLoad(30)

//WebUI.click(findTestObject('Object Repository/SiteCore/HomePage/cookiesToastContainer'))

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_Healthcare Security Certification  HCI_566946/a_HCISPP                                   _33b0e2'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Healthcare Security Certification  HCI_566946/h1_HCISPP  The HealthCare Security Certification'), 
    'HCISPP – The HealthCare Security Certification')

//WebUI.closeBrowser()

//WebUI.openBrowser(GlobalVariable.URL)

WebUI.waitForPageLoad(30)

WebUI.waitForElementVisible(findTestObject('SiteCore/HomePage/a_Certifications'), 30)

WebUI.mouseOver(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/a_Certifications'))

WebUI.click(findTestObject('SiteCore/HomePage/Page_IT Security Architect Engineer and Man_20c9f5/a_Associate of (ISC)                       _63941e'))

WebUI.verifyElementText(findTestObject('SiteCore/CertificationsPages/LatestCertificationPages/Page_Path to Entry Level Cybersecurity Cert_49c079/h1_Start Your Cybersecurity Career'), 
    'Start Your Cybersecurity Career')

WebUI.closeBrowser()

