import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.remote.server.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.ExpectedConditions as ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait as WebDriverWait
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.junit.After as After
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

Date today = new Date()

String todaysDate = today.format('MM-dd-yyyy')

String currentTime = today.format('hh-mm-ss')

String actualName = new String((((('LF' + todaysDate)))+ '-') + currentTime)

int SHORT_TIMEOUT = 10

int TIMEOUT = 20

int LONG_TIMEOUT = 30

String currentDirectory = System.getProperty('user.dir')

String email = new String(((('LF' + todaysDate) + '-') + currentTime) + '@mailinator.com')

WebUI.openBrowser('')

WebDriver driver = DriverFactory.getWebDriver()

WebDriverWait waitForVisibilityOfElement = new WebDriverWait(driver, LONG_TIMEOUT)

WebUI.navigateToUrl(GlobalVariable.LeadFormURL)

//waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='close button']")))
//WebUI.click(findTestObject('SiteCore/HomePage/cookiesToastContainer'))
//waitForVisibilityOfElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath('//input[@id=\'wffma6c293203e1b4fc5be8b826edf4d0ab2_Sections_0__Fields_0__Value\']')))

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/firstNameField'), actualName)
println ("first name:- " + actualName)

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/lastNameField'), 'Lead Form')

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/jobTitleField'), 'AutomatedLeadForm')

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/companyField'), 'Automation Test')

//WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_1') + 
 //   todaysDate) + '-') + currentTime) + '.PNG')

WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownTypeOfIndustry'), 'Agriculture', true)

WebUI.scrollToElement(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/scrollEmailField'), 3)

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/scrollEmailField'), email)

WebUI.scrollToElement(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/addressCountryField'), 3)

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/phoneNumberField'), '1234567899')

WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/addressCountryField'), 'Australia', true)

//WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_2') + 
  //  todaysDate) + '-') + currentTime) + '.PNG')

result = WebUI.getText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/scrollEmailField'))

WebUI.setText(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/addressStateField'), 'MH')

WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownCertificationOfInterest'), 'CSSLP', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownNoOfEmployees'), '10-19', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/dropdownTrainingTimeline'), '4-6 Months', true)

WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/agreePolicyCheckbox'))

//WebUI.click(findTestObject('Object Repository/org.isc2.aqa.sitecore.pageObjects/LeadPages/submitButton'))

WebUI.delay(10)

WebUI.verifyTextPresent('Thank you for filling in the form.', false)

WebUI.takeScreenshot(((((currentDirectory + 'KatalonTestScreenshots/TestCase-LeadForm/Scenario-VerifyLeadFormCreation/FormFilledContent_2') + 
    todaysDate) + '-') + currentTime) + '.PNG')

//WebUI.navigateToUrl('https://www.isc2.org/api/coretest/endsession')

WebUI.delay(10)

WebUI.closeBrowser()


