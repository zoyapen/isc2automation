<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CISSP Concentrations                     _d8be5c</name>
   <tag></tag>
   <elementGuidId>896c80c5-f4cd-45e7-8117-45d0367d80b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@href, '/Certifications/CISSP-Concentrations')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/Certifications/CISSP-Concentrations</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CISSP Concentrations                                                                                        Architecture, Engineering, and Management Concentrations
                                                                                    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-grid&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;dropdown yamm-fw open&quot;]/ul[@class=&quot;dropdown-menu on&quot;]/li[@class=&quot;grid&quot;]/div[@class=&quot;mm-item-v2&quot;]/div[@class=&quot;flx flx-main&quot;]/div[@class=&quot;flx-col cnt&quot;]/div[@class=&quot;flx flx-cat&quot;]/div[@class=&quot;flx-col cat col-sm-6&quot;]/ul[@class=&quot;nav&quot;]/li[2]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-grid']/ul/li[2]/ul/li/div/div/div[2]/div/div[3]/ul/li[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Healthcare'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CISSP Concentrations']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/Certifications/CISSP-Concentrations')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div/div[3]/ul/li[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
