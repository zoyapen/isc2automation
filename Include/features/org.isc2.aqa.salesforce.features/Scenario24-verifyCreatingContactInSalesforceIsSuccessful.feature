Feature: Scenario-25 Creating a new contact in salesforce is successful

  Scenario Outline: Create New Contact from Salesforce using Email that exist in Salesforce
  	Given I open a browser
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to contacts page
    And I create a new contact
    And I enter first name as <firstNameValue>
    And I enter last name as <lastNameValue>
    And I enter email address
    And I click save
    Then I see the correct account name
    And I click the drop-down to perform advanced opertions
    And I disable customer user
    And I click disable customer user
    And I click the drop-down to perform advanced opertions
    And I click the delete button from the drop-down list
    And I delete the user
    Then I see the user is deleted successfully
    And I close a browser
    	Examples:
    		|firstNameValue|lastNameValue|
    		|Ashwin18			 |I18					 | 
    		