Feature: Training finder search works as expected

  Scenario Outline: Verify that training finder search works as expected
    Given I open a browser
    And I navigate to training finder search
    And I click Advanced filters
    And I select certification type from the advanced filters drop-down
    And I select country from the advanced filters drop-down
    And I search for the above training
    Then I see CISSP Bootcamp Seminar in the training solution search results
    And I see location as Spain for one of the trainings
