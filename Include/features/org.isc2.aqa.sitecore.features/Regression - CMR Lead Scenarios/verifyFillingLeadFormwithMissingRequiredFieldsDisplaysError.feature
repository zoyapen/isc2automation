Feature: Scenario- Filling a Lead Form with missing required fields displays error

  Scenario: Verify that filling a lead form with missing required fields displays error
    Given I open a browser
    When I navigate to private-on-site lead form
  	And I submit private-on-site lead form
  	Then I see error message for missing first name in private-on-site lead form
  	And I see error message for missing last name in private-on-site lead form
  	And I see error message for missing job title in private-on-site lead form
  	And I see error message for missing company field in private-on-site lead form
  	And I see error message for missing email in private-on-site lead form
  	And I see error message for Privacy policy in private-on-site lead form
  	And I see error message for missing phone number field in private-on-site lead form
  	And I see error message for missing country or region field in private-on-site lead form
  	And I see error message for missing certification of interest field in private-on-site lead form
  	And I see error message for missing no of employees to be trained field in private-on-site lead form
  	And I see error message for missing timeline for training field in private-on-site lead form
  
  Scenario: Verify that filling a lead form with all the required details does not display error
    Given I open a browser
    When I navigate to private-on-site lead form
 		And I enter the first name in private-on-site lead form
 		And I enter the last name in private-on-site lead form
 		And I enter the job title in private-on-site lead form
 		And I enter the company in private-on-site lead form
 		And I select the type of industry in private-on-site lead form
 		And I enter the email in private-on-site lead form
 		And I enter the phone number in private-on-site lead form
 		And I select Country in private-on-site lead form
 		And I select State or Province in private-on-site lead form
  	And I select Ceritification of Interest in private-on-site lead form
  	And I select Number of Employees to be trained in private-on-site lead form
  	And I select Time for Training in private-on-site lead form 
  	And I have read and agree to the privacy policy in private-on-site lead form
  	Then I do not see error message for missing first name in private-on-site lead form
  	And I do not see error message for missing last name in private-on-site lead form
  	And I do not see error message for missing job title in private-on-site lead form
  	And I do not see error message for missing company field in private-on-site lead form
  	And I do not see error message for missing email in private-on-site lead form
  	And I do not see error message for Privacy policy in private-on-site lead form
  	And I do not see error message for missing phone number field in private-on-site lead form
  	And I do not see error message for missing country or region field in private-on-site lead form
  	And I do not see error message for missing certification of interest field in private-on-site lead form
  	And I do not see error message for missing no of employees to be trained field in private-on-site lead form
  	And I do not see error message for missing timeline for training field in private-on-site lead form
  
    
    
    
  	
