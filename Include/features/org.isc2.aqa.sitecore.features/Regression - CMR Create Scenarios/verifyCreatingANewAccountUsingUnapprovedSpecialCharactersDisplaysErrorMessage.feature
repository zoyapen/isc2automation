Feature: Scenario-9 Creating a New User Account using unapproved special characters in first name and last name displays error message

  Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I/SC2First			 	 |I/SC2Last 	 		 |
 
   Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I\SC2First 			 |I\SC2Last 		   |
 
  Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I<SC2First   		 |I<SC2Last  			 |
 
   Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I>SC2First   		 |I>SC2Last		     |
    		
    Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I"SC2First 			 |I"SC2Last   		 |
  
   Scenario Outline: Verify that creating a New User Account using unapproved special characters displays error message
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name using <firstNameValue>
    And I enter last name using <lastNameValue>
    And I enter an email for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see error message stating special characters are not allowed in first name field
    And I see error message stating special characters are not allowed in last name field 
    	Examples:
    		|firstNameValue		 |lastNameValue 	 |
    		|I,SC2First        |I,SC2Last        |
    		
    		