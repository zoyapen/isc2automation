Feature: Creating a New User Account with Account info that exist in SF and not in Okta fails account creation

  Scenario Outline: Verify that creating a New User Account with Account info that exist in SF and not in Okta fails account creation
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter last name for creating an account
    And I enter email address as <emailAddressValue> 
    And I confirm email address as <emailAddressValue>
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see account creation fails
    	Examples:
    		|emailAddressValue														|
    		|DoNotDeleteUsedForAutomation@mailinator.com	|
