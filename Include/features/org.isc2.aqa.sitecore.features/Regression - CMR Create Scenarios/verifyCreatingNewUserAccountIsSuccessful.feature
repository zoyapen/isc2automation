Feature: Scenario-1 Creating a New User Account

  Scenario : Verify that creating a New User Account is successful 
    Given I open a browser
    When I'm on sitecore
    And I click sign-in button on home page
    And I click the create an account button
    And I enter first name for creating an account
    And I enter an email for creating an account
    And I enter last name for creating an account
    And I confirm email for creating an account
    And I enter passwrd for creating an account
    And I confirm password for creating an account
    And I agree to the privacy policy
    And I click create a new account button
    Then I see a new account is been created for the above user
    And I end the session
    When I'm on salesforce
    And I enter the login credentials
    And I click the login button
    Then I see the login is successful
    And I navigate to automated contacts page
    And I search for the contact that was just submitted
    Then I see the correct email for the above contact
    And I see the correct contact owner as mciamser
    Then I see CREATED for Okta status
    And I see CREATED for WebDB status
    And I see CREATED for Salesforce status 
		And I close a browser